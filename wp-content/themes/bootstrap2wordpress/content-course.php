<?php

$feature_course_img   = get_field('feature_course_img');
$feature_course_title = get_field('feature_course_title');
$feature_course_body  = get_field('feature_course_body');

?>





<!-- COURSE FEATURES
	================================================== -->
    <section id="course-features">
    <div class="container">

        <div class="section-header">

            <?php if( !empty($feature_course_img) ) : ?>

            <img src="<?php echo $feature_course_img['url']; ?>" alt="<?php echo $feature_course_img['alt'];?>">

            <?php endif ?>

            <h2><?php echo $feature_course_title ?></h2>

            <?php if ( !empty($feature_course_body) ) : ?>

            <p class="lead"><?php echo $feature_course_body ?></p>

            <?php endif; wp_reset_query(); ?>

        </div><!-- section-header -->

        <div class="row">

            <?php $loop = new WP_Query(array('post_type' => 'course_feature', 'orderby' => 'post_id','order' => 'ASC')); ?>

            <?php while($loop -> have_posts()) : $loop -> the_post(); ?>

            <div class="col-sm-2">
                <i class="<?php the_field('course_feature_icon'); ?>"></i>
                <h4><?php the_title(); ?></h4>
            </div><!-- end col -->

            <?php endwhile ?>


        </div><!-- row -->
    </div><!-- container -->
</section><!-- course-features -->

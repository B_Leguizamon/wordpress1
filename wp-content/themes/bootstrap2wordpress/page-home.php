<?php

/*
    Template Name: Home Page
*/



/*ADVANCED CUSTOM FIELDS*/












get_header();
?>

<?php get_template_part('content','hero'); ?>


<?php get_template_part('content','optin'); ?>

<?php get_template_part('content','boost'); ?>

<?php get_template_part('content','benefits'); ?>

<?php get_template_part('content','course'); ?>

<?php get_template_part('content','features'); ?>

<?php get_template_part('content','video'); ?>

<?php get_template_part('content','instructor'); ?>

<?php get_template_part('content','testimonials'); ?>







<?php
get_footer();
?>
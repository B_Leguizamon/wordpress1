<?php

$video_course_title   = get_field('video_course_title');
$video_course_vid     = get_field('video_course_vid');

?>



<!-- VIDEO FEATURETTE
	================================================== -->
    <section id="featurette">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2><?php echo $video_course_title; ?></h2>
                <iframe width="100%" height="415" src="<?php echo $video_course_vid; ?>" frameborder="0"
                    allowfullscreen></iframe>
            </div><!-- end col -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- featurette -->